import { TaskElement } from './elements/task.element';

class TaskListComponent {
    get view() {
        return cy.get('div[role="listbox"]');
    }

    get addTaskButton() {
        return this.view.find('.plus_add_button');
    }

    public taskItem(taskName: string) {
        return new TaskElement(taskName);
    }
}

export const taskListComponent = new TaskListComponent();
