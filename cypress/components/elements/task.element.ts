import { TaskActionsType } from 'cypress/types/TaskActionsType';

export class TaskElement {
    private taskName: string;
    constructor(taskName: string) {
        this.taskName = taskName;
    }
    get view() {
        return cy.get('.task_list_item').contains(this.taskName).parentsUntil('.task_list_item').last();
    }
    get contentText() {
        return cy.get('.task_list_item').contains(this.taskName);
    }
    get menuView() {
        return cy.get('.popper');
    }
    public openMenu() {
        return this.view.rightclick();
    }
    public action(taskAction: TaskActionsType) {
        if (taskAction === 'complete') {
            return this.view.find('button[data-action-hint="task-complete"]').click();
        }
        this.openMenu();
        return cy.get(`li[data-action-hint="task-overflow-menu-${taskAction}"]`).click();
    }
}
