class EditTaskComponent {
    get view() {
        return cy.get('.task_editor');
    }

    get contentNameField() {
        return cy.get('.task_editor__content_field');
    }

    get descriptionField() {
        return cy.get('.task_editor__description_field');
    }

    public save() {
        return cy.get('button[type="submit"]').click();
    }

    public cancel() {
        return cy.get('.reactist_button--secondary').click();
    }

    public setData(taskDetails: IToDoTask) {
        this.contentNameField.clear();
        this.contentNameField.type(taskDetails.name);
    }
}

export const editTaskComponent = new EditTaskComponent();
