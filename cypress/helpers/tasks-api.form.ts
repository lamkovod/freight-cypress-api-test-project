import { v4 as uuidv4 } from 'uuid';

export const addTaskRequest = (taskItem: ITaskItem) => {
    return {
        method: 'POST',
        url: '/sync',
        auth: {
            bearer: Cypress.env('token'),
        },
        body: {
            commands: [
                {
                    type: 'item_add',
                    uuid: uuidv4(),
                    temp_id: uuidv4(),
                    args: taskItem,
                },
            ],
        },
    };
};

export const updateTaskRequest = (taskItem: ITaskItemUPD) => {
    return {
        method: 'POST',
        url: '/sync',
        auth: {
            bearer: Cypress.env('token'),
        },
        body: {
            commands: [
                {
                    type: 'item_update',
                    uuid: uuidv4(),
                    args: taskItem,
                },
            ],
        },
    };
};

export const completeTaskRequest = (taskItem: { id: string; date_completed?: string }) => {
    return {
        method: 'POST',
        url: '/sync',
        auth: {
            bearer: Cypress.env('token'),
        },
        body: {
            commands: [
                {
                    type: 'item_complete',
                    uuid: uuidv4(),
                    args: taskItem,
                },
            ],
        },
    };
};
