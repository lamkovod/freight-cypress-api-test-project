import { editTaskComponent } from 'cypress/components/editTask.component';
import { taskListComponent } from 'cypress/components/taskList.component';

class TasksActions {
    public assertTaskwithNameDisplayed(taskName: string) {
        taskListComponent.taskItem(taskName).view.should('be.visible');
    }

    public assertTaskwithNameNotDisplayed(taskName: string) {
        taskListComponent.taskItem(taskName).contentText.should('not.exist');
    }

    public createNewTask(todoTask: IToDoTask) {
        taskListComponent.addTaskButton.click();
        editTaskComponent.setData(todoTask);
        editTaskComponent.save();
    }

    public editTask(taskName: string, todoTask: IToDoTask) {
        taskListComponent.taskItem(taskName).action('edit');
        editTaskComponent.setData(todoTask);
        editTaskComponent.save();
    }

    public completeTask(taskName: string) {
        taskListComponent.taskItem(taskName).action('complete');
    }
}

export const tasksActions = new TasksActions();
