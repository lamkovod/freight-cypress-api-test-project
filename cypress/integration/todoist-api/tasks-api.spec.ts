import { addTaskRequest, completeTaskRequest, updateTaskRequest } from 'cypress/helpers/tasks-api.form';

describe('API Tasks', () => {
    describe('User can create', () => {
        const createdTaskName = 'firstApiTest';
        it('a new todo item with date and content', () => {
            cy.request(
                addTaskRequest({ content: createdTaskName, project_id: Cypress.env('projectId').toString() }),
            ).should((response) => {
                expect(response.status).to.eq(200);
            });
        });

        after('remove created task', () => {
            cy.removeTask(createdTaskName);
        });
    });
    describe('User can edit task', () => {
        const startTaskName = 'taskToRenameAPI';
        const finishTaskName = 'renamedTaskAPI';

        beforeEach('create task with start name', () => {
            cy.createTask({ name: startTaskName });
        });
        it('rename todo item', () => {
            cy.request(updateTaskRequest({ id: Cypress.env('createdTaskId'), content: finishTaskName })).should(
                (response) => {
                    expect(response.status).to.eq(200);
                },
            );
        });

        afterEach('remove edited task', () => {
            cy.removeTask(finishTaskName);
        });
    });

    describe('User can complete', () => {
        const defaultTaskName = 'defaultTaskAPI';

        beforeEach('create default task', () => {
            cy.createTask({ name: defaultTaskName });
        });

        it('complete todo item', () => {
            cy.request(completeTaskRequest({ id: Cypress.env('createdTaskId') })).should((response) => {
                expect(response.status).to.eq(200);
            });
        });

        afterEach('remove default task', () => {
            cy.removeTask(defaultTaskName, true);
        });
    });
});
