import { tasksActions } from 'cypress/actions/tasks.actions';

describe('ToDo Tasks Critical', () => {
    beforeEach('Open Test Project page', () => {
        cy.setCookie('todoistd', Cypress.env('tempCookie'), {
            path: '/',
            domain: '.todoist.com',
        });
        cy.visit('/app/project/' + Cypress.env('projectId'));
    });

    describe('User can create', () => {
        const createdTaskName = 'firstTest';
        it('a new todo item with date and content', () => {
            const taskData = { name: createdTaskName };
            tasksActions.createNewTask(taskData);
            tasksActions.assertTaskwithNameDisplayed(taskData.name);
        });

        after('remove created task', () => {
            cy.removeTask(createdTaskName);
        });
    });
    describe('User can rename', () => {
        const startTaskName = 'taskToRename';
        const finishTaskName = 'renamedTask';

        beforeEach('create task with start name', () => {
            cy.createTask({ name: startTaskName });
            cy.reload();
        });
        it('todo item', () => {
            const taskData = { name: finishTaskName };
            tasksActions.editTask(startTaskName, taskData);
            tasksActions.assertTaskwithNameDisplayed(finishTaskName);
            tasksActions.assertTaskwithNameNotDisplayed(startTaskName);
        });

        afterEach('remove renamed task', () => {
            cy.removeTask(finishTaskName);
        });
    });

    describe('User can complete', () => {
        const defaultTaskName = 'defaultTask';

        beforeEach('create default task', () => {
            cy.createTask({ name: defaultTaskName });
            cy.reload();
        });

        it('complete todo item', () => {
            tasksActions.completeTask(defaultTaskName);
            tasksActions.assertTaskwithNameNotDisplayed(defaultTaskName);
        });

        afterEach('remove default task', () => {
            cy.removeTask(defaultTaskName);
        });
    });
});
