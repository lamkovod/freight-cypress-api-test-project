export type TaskActionsType = 'edit' | 'delete' | 'complete';
