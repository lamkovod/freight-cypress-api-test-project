declare interface IToDoTask {
    readonly name: string;
    readonly description?: string;
    readonly date?: string;
}
