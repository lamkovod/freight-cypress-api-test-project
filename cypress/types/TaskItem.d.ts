declare interface ITaskItem {
    readonly content: string;
    readonly description?: string;
    readonly project_id?: string;
    readonly due?: string;
    // ...
}
