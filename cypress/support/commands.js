import { v4 as uuidv4 } from 'uuid';

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
const todoIstApiURL = 'https://api.todoist.com/sync/v8/';

Cypress.Commands.add('createTask', (taskData) => {
    Cypress.log({
        name: 'postTask',
    });

    const options = {
        method: 'POST',
        url: todoIstApiURL + 'items/add',
        auth: {
            bearer: Cypress.env('token'),
        },
        body: { content: taskData.name, project_id: Cypress.env('projectId').toString() },
    };
    cy.request(options)
        .its('body')
        .then((body) => {
            Cypress.env('createdTaskId', body.id);
        });
});

Cypress.Commands.add('removeTask', (taskName, completed = false) => {
    Cypress.log({
        name: 'deleteTask',
    });

    let getRequestOptions = {
        method: 'POST',
        url: todoIstApiURL + 'projects/get_data',
        auth: { bearer: Cypress.env('token') },
        body: { project_id: Cypress.env('projectId').toString() },
    };

    if (completed === true) {
        getRequestOptions = {
            method: 'POST',
            url: todoIstApiURL + 'completed/get_all',
            auth: { bearer: Cypress.env('token') },
        };
    }
    const numberOfRetries = 15;
    let retries = numberOfRetries;

    function tryToDeleteTask() {
        retries--;
        return cy
            .request(getRequestOptions)
            .its('body')
            .then((body) => {
                let taskId = undefined;
                try {
                    taskId = body.items.find((item) => item.content === taskName).id;
                } catch (err) {
                    if (retries === 0) {
                        throw new Error(`Retried to delete task ${numberOfRetries} times!`);
                    }
                    // eslint-disable-next-line cypress/no-unnecessary-waiting
                    cy.wait(1000);
                    return tryToDeleteTask();
                }
                const options = {
                    method: 'POST',
                    url: todoIstApiURL + 'sync',
                    auth: {
                        bearer: Cypress.env('token'),
                    },
                    body: {
                        commands: [{ type: 'item_delete', uuid: uuidv4(), args: { id: taskId } }],
                    },
                };
                return cy.request(options);
            });
    }

    return tryToDeleteTask();
});
