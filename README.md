# Freight Cypress API Test Project

Cypress + API Tests for Test Project

### Setup

```
git clone git@gitlab.com:lamkovod/freight-cypress-api-test-project.git
cd freight-cypress-api-test-project
npm i
```

### Create user for Todoist and choose project

Create new file `cypress.env.json` with sensitive data for your todoist user.
Example:

```
{
    "email": "exampl@mail.com",
    "password": "1234567889",
    "tempCookie":
        "cookie_val_after_logged_in",
    "token": "d81a1cc3dac1743cdbc31fb3d_user_token",
    "projectId": "12345"
}
```

### Launching tests

Launch API suite:
```
npx cypress run --config-file cypress-api.json
```
Launch UI suite:
```
npx cypress run
```
### Gitlab Pages Report

https://lamkovod.gitlab.io/freight-cypress-api-test-project/

### Tech

-   [Cypress](https://github.com/cypress-io/cypress)
-   [Mocha](https://github.com/mochajs/mocha)
-   [Mochawesome](https://github.com/adamgruber/mochawesome)
-   [Allure](https://github.com/allure-framework)
-   [TypeScript](https://github.com/microsoft/TypeScript)
